package com.example.shopping_lists.datalayer.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.example.shopping_lists.utils.BooleanToIntegerConverter;

import org.json.JSONException;
import org.json.JSONObject;

@Entity
public abstract class SynchronizableEntity {
    @PrimaryKey
    @NonNull
    protected String id;
    @NonNull
    @TypeConverters(BooleanToIntegerConverter.class)
    protected boolean synched;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSynched() {
        return synched;
    }

    public void setSynched(boolean synched) {
        this.synched = synched;
    }

    public SynchronizableEntity(@NonNull String id, boolean synched) {
        this.id = id;
        this.synched = synched;
    }

    public JSONObject toJSONobject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        jsonObject.put("synched", synched);
        return jsonObject;
    }


}
