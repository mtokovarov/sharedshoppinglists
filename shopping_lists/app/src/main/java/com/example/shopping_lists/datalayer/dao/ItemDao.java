package com.example.shopping_lists.datalayer.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.example.shopping_lists.datalayer.entities.Item;
import com.example.shopping_lists.datalayer.entities.ItemWithAuthor;

import java.util.List;

@Dao
public abstract class ItemDao extends SynchronizableEntityDao<Item> {
    @Query("SELECT * FROM Item i LEFT JOIN User u ON i.authorId=u.userId WHERE i.shoppingListId = :shoppingListId ORDER BY strftime('%s',i.datetimeCreated) DESC")
    public abstract LiveData<List<ItemWithAuthor>> getItemsWithAuthorsFromNewest(String shoppingListId);
    @Query("SELECT * FROM Item i LEFT JOIN User u ON i.authorId=u.userId" +
            " WHERE i.shoppingListId = :shoppingListId AND authorId = :authorId ORDER BY strftime('%s',i.datetimeCreated) DESC")
    public abstract LiveData<List<ItemWithAuthor>> getItemsWithAuthorsOfSelectedAuthorFromNewest(String shoppingListId, String authorId);
}
