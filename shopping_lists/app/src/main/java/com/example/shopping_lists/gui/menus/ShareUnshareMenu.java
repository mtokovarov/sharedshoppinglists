package com.example.shopping_lists.gui.menus;

import android.view.Menu;
import android.view.MenuInflater;

import com.example.shopping_lists.R;



public class ShareUnshareMenu {
    public ShareUnshareMenu(MenuInflater inflater, Menu menu) {
        inflater.inflate(R.menu.menu_sharing_unsharing, menu);
    }
}
