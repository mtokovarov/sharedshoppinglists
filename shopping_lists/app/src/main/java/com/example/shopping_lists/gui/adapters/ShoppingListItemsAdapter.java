package com.example.shopping_lists.gui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.datalayer.entities.Item;
import com.example.shopping_lists.datalayer.entities.ItemWithAuthor;
import com.example.shopping_lists.datalayer.entities.User;
import com.example.shopping_lists.gui.dialogs.EditItemDialog;
import com.example.shopping_lists.gui.spinners.UsersWithDefaultAllSpinner;

import java.time.format.DateTimeFormatter;
import java.util.List;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class ShoppingListItemsAdapter extends RecyclerView.Adapter<ShoppingListItemsAdapter.ItemViewHolder> {
    private List<ItemWithAuthor> itemsWithAuthorsList;
    private LiveData<List<ItemWithAuthor>> itemsWithAuthorsListLiveData;
    private UsersWithDefaultAllSpinner usersWithDefaultAllSpinner;
    private Context context;
    private String selectedShoppingListId;

    public ShoppingListItemsAdapter(Context context, String selectedShoppingListId) {
        this.context = context;
        this.selectedShoppingListId = selectedShoppingListId;
        initUserSpinner();
    }

    private void initUserSpinner(){
        usersWithDefaultAllSpinner = new UsersWithDefaultAllSpinner(context, R.id.itemUsersSpinner);
        usersWithDefaultAllSpinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = usersWithDefaultAllSpinner.getSelectedUserOrNull();
                if(itemsWithAuthorsListLiveData != null)
                    itemsWithAuthorsListLiveData.removeObservers((LifecycleOwner) context);
                if (selectedUser == null)
                    itemsWithAuthorsListLiveData = MainActivity.getDatabase().getItemDao().getItemsWithAuthorsFromNewest(selectedShoppingListId);
                else
                    itemsWithAuthorsListLiveData = MainActivity.getDatabase().getItemDao().getItemsWithAuthorsOfSelectedAuthorFromNewest(selectedShoppingListId, selectedUser.getUserId());
                itemsWithAuthorsListLiveData.observe((LifecycleOwner) context, new Observer<List<ItemWithAuthor>>() {
                    @Override
                    public void onChanged(List<ItemWithAuthor> itemWithAuthors) {
                        itemsWithAuthorsList = itemWithAuthors;
                        notifyDataSetChanged();
                    }
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_list_items_adapter, parent, false);
        return new ItemViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        initHolderTextAndVisualElements(holder, position);
        setDeleteButton(holder, position);
        setEditButton(holder, position);
        setFinishedChanged(holder, position);
        setOnClickListenerOnHolder(holder, position);
    }


    @Override
    public int getItemCount() {
        if (itemsWithAuthorsList == null)
            return 0;
        return itemsWithAuthorsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        public TextView product;
        public TextView quantity;
        public TextView authorFirstName;
        public TextView getAuthorLastName;
        public TextView createdDateTime;
        public CheckBox isFinished;
        public ImageView isSynched;
        public Button editButton;
        public Button deleteButton;
        public View.OnClickListener onClickListener;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            product = itemView.findViewById(R.id.itemProduct);
            quantity = itemView.findViewById(R.id.itemQuantity);
            authorFirstName = itemView.findViewById(R.id.itemAuthorFirstName);
            getAuthorLastName = itemView.findViewById(R.id.itemAuthorLastName);
            createdDateTime = itemView.findViewById(R.id.itemDateTimeCreated);
            isFinished = itemView.findViewById(R.id.itemtFinishedCheckBox);
            isSynched = itemView.findViewById(R.id.isSynchedItem);
            editButton = itemView.findViewById(R.id.editItemButton);
            deleteButton = itemView.findViewById(R.id.deleteItemButton);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initHolderTextAndVisualElements(ItemViewHolder holder, int position){
        ItemWithAuthor itemWithAuthor = itemsWithAuthorsList.get(position);
        holder.product.setText(itemWithAuthor.item.getProduct());
        holder.quantity.setText(itemWithAuthor.item.getQuantity());
        holder.authorFirstName.setText(itemWithAuthor.author.getFirstName());
        holder.getAuthorLastName.setText(itemWithAuthor.author.getLastName());
        holder.createdDateTime.setText(itemWithAuthor.item.getDatetimeCreated().format(DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss")));
        holder.isFinished.setChecked(itemWithAuthor.item.isFinished());
        if (itemWithAuthor.item.isSynched())
            holder.isSynched.setImageResource(android.R.drawable.stat_sys_upload_done);
        else
            holder.isSynched.setImageResource(android.R.drawable.ic_popup_sync);
    }

    private void setDeleteButton(ItemViewHolder holder, final int position){
        ItemWithAuthor itemWithAuthor = itemsWithAuthorsList.get(position);
        if (!itemWithAuthor.item.getAuthorId().contentEquals(MainActivity.getOwner().getUserId())){
            holder.deleteButton.setVisibility(View.INVISIBLE);
            return;
        }
        holder.deleteButton.setVisibility(View.VISIBLE);
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Confirm");
                builder.setMessage("Do you really want to delete the item?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void onClick(DialogInterface dialog, int which) {
                        Item itemToDelete = itemWithAuthor.item;
                        MainActivity.getDatabase().getItemDao().delete(itemToDelete).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {}

                            @Override
                            public void onError(Throwable e) {}
                        });
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void setEditButton(ItemViewHolder holder, final int position){
        ItemWithAuthor itemWithAuthor = itemsWithAuthorsList.get(position);
        if (!itemWithAuthor.item.getAuthorId().contentEquals(MainActivity.getOwner().getUserId())){
            holder.deleteButton.setVisibility(View.INVISIBLE);
            return;
        }
        holder.deleteButton.setVisibility(View.VISIBLE);
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new EditItemDialog(context, itemsWithAuthorsList.get(position).item).show();
            }
        });
    }

    private void setFinishedChanged(ItemViewHolder holder, final int position){
        holder.isFinished.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Item changedItem = itemsWithAuthorsList.get(position).item;
                changedItem.setFinished(holder.isFinished.isChecked());
                changedItem.setSynched(false);
                MainActivity.getDatabase().getItemDao().update(changedItem).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

            }
        });

    }

    @Override
    public void onViewRecycled(@NonNull ItemViewHolder holder) {
        holder.isFinished.setOnCheckedChangeListener(null);
        super.onViewRecycled(holder);
    }

    private void setOnClickListenerOnHolder(ItemViewHolder holder, final int position){
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.isFinished.toggle();
            }
        });
    }
}
