package com.example.shopping_lists.datalayer.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.example.shopping_lists.datalayer.entities.User;

import java.util.List;

import io.reactivex.Single;

@Dao
public abstract class UserDao {
    @Query("SELECT * FROM User WHERE userId = :id")
    public abstract Single<User> getUserById(String id);

    @Query("SELECT * FROM User")
    public abstract LiveData<List<User>> getAllUsers();

    @Query("SELECT * FROM User")
    public abstract LiveData<List<User>> getNonSynched();

    @Query("SELECT * FROM User WHERE isOwner = 1")
    public abstract Single<User> getOwner();

    @Query("SELECT * FROM User WHERE isOwner = 0")
    public abstract LiveData<List<User>> getUsersWithoutOwner();


}
