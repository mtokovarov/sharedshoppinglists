package com.example.shopping_lists.datalayer.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.example.shopping_lists.datalayer.entities.Product;

import java.util.List;

import io.reactivex.Single;

@Dao
public abstract class ProductDao{
    @Query("SELECT * FROM Product WHERE id = :id ORDER BY name")
    public abstract Single<Product> getProductById(String id);

    @Query("SELECT * FROM Product ORDER BY name")
    public abstract Single<List<Product>> getAllProducts();

    @Query("SELECT * FROM Product WHERE categoryId = :id ORDER BY name")
    public abstract Single<List<Product>> getProductsByCategoryId(String id);

    @Query("SELECT * FROM Product WHERE name = :name")
    public abstract Single<Product> getProductByName(String name);
}
