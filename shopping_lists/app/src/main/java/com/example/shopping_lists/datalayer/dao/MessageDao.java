package com.example.shopping_lists.datalayer.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.example.shopping_lists.datalayer.entities.Message;

import java.util.List;

@Dao
public abstract class MessageDao extends SynchronizableEntityDao<Message> {
    @Query("SELECT * FROM Message WHERE userFrom = :userId OR userTo = :userId ORDER BY strftime('%s',datetimeSent) ASC LIMIT 1000")
    public abstract LiveData<List<Message>> getMessagesToAndFromUserIdFromOlders(String userId);
}
