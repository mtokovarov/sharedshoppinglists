package com.example.shopping_lists.datalayer.dao;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.datalayer.entities.Modification;
import com.example.shopping_lists.datalayer.entities.SynchronizableEntity;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

@Dao
public abstract class SynchronizableEntityDao <T extends SynchronizableEntity>{
    @Insert
    public abstract Completable simpleInsert(T t);
    @Update
    public abstract Completable simpleUpdate(T t);
    @Delete
    public abstract Completable simpleDelete(T t);
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Insert

    public void registerModification(T t, String modificationType){
        String tableName = t.getClass().getSimpleName();
        String rowId = t.getId();
        Modification mod = new Modification(tableName, rowId, modificationType);
        MainActivity.getDatabase().getModificationDao().insert(mod).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {
                throw new RuntimeException("Modofication not registered!");
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public Completable insert(T t){
        registerModification(t, "insert");
        return simpleInsert(t);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Completable update(T t){
        registerModification(t, "update");
        return simpleUpdate(t);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Completable delete(T t){
        registerModification(t, "delete");
        return simpleDelete(t);
    }

    public String getTableName(){
        Class cls = (Class)((ParameterizedType)getClass().getSuperclass().getGenericSuperclass()).getActualTypeArguments()[0];
        return cls.getSimpleName();
    }


    @RawQuery
    abstract Single<String[]> getStringsByRawQuery(SupportSQLiteQuery sqLiteQuery);

    @RawQuery
    public abstract Single<List<T>> getDataByRawQuery(SupportSQLiteQuery sqLiteQuery);

    @RawQuery
    public abstract Single<Object> runRawQuery(SupportSQLiteQuery sqLiteQuery);

    public Single<List<T>> getInserted(){
        String queryText = "SELECT * FROM " + getTableName() + " WHERE id IN " +
                "(SELECT rowId FROM Modification WHERE tableName = \"" + getTableName() + "\" AND modificationType = \"insert\") "+
                "AND "+
                "id NOT IN (SELECT rowId FROM Modification WHERE tableName = \"" + getTableName() + "\" AND modificationType = \"delete\")";
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryText);
        return getDataByRawQuery(query);
    }

    public Single<List<T>> getUpdated(){
        String queryText = "SELECT * FROM " + getTableName() + " WHERE id IN " +
                "(SELECT rowId FROM Modification WHERE tableName = \"" + getTableName() + "\" AND modificationType = \"update\") "+
                "AND "+
                "id NOT IN (SELECT rowId FROM Modification WHERE tableName = \"" + getTableName() + "\" AND modificationType = \"delete\") "+
                "AND "+
                "id NOT IN (SELECT rowId FROM Modification WHERE tableName = \"" + getTableName() + "\" AND modificationType = \"insert\")";
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryText);
        return getDataByRawQuery(query);
    }


    public Single<String[]> getDeleted(){
        String queryText = "SELECT rowId FROM Modification WHERE tableName = \"" + getTableName() + "\" AND modificationType = \"delete\" "+
                "AND rowId NOT IN " +
                "(SELECT rowId FROM Modification WHERE tableName = \"" + getTableName() + "\" AND modificationType = \"insert\")";
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryText);
        return getStringsByRawQuery(query);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Single<Object> setSynchedTrue(String[] ids){
        String queryText = "UPDATE " + getTableName() + " SET synched = 1 WHERE id IN (" + String.join(", ", ids) + ")";
        return runRawQuery(new SimpleSQLiteQuery(queryText));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Single<Object> setSynchedTrue(List<T> tList){
        String[] ids = new String[tList.size()];
        for (int i = 0; i<tList.size();i++)
            ids[i] = tList.get(i).getId();
        return setSynchedTrue(ids);
    }
}
