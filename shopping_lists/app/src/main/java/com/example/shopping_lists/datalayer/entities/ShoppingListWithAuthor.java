package com.example.shopping_lists.datalayer.entities;

import androidx.room.Embedded;
import androidx.room.Relation;

public class ShoppingListWithAuthor {
    @Embedded
    public ShoppingList shoppingList;
    @Relation(
            parentColumn  = "authorId",
            entityColumn = "userId"
    )
    public User author;
}
