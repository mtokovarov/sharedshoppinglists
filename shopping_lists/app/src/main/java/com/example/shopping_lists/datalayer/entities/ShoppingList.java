package com.example.shopping_lists.datalayer.entities;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.TypeConverters;

import com.example.shopping_lists.utils.BooleanToIntegerConverter;
import com.example.shopping_lists.utils.DateTimeZoneConverter;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static androidx.room.ForeignKey.SET_NULL;

@Entity(foreignKeys = @ForeignKey(entity = User.class, parentColumns = "userId", childColumns = "authorId", onDelete = SET_NULL))
public class ShoppingList extends SynchronizableEntity {
    @NonNull
    private String name;
    private String authorId;
    @NonNull
    @TypeConverters(DateTimeZoneConverter.class)
    private ZonedDateTime datetimeCreated;
    @NonNull
    @TypeConverters(BooleanToIntegerConverter.class)
    private boolean finished;

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    @NonNull
    public ZonedDateTime getDatetimeCreated() {
        return datetimeCreated;
    }

    public void setDatetimeCreated(@NonNull ZonedDateTime datetimeCreated) {
        this.datetimeCreated = datetimeCreated;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }


    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }


    public ShoppingList(@NonNull String id, boolean synched, @NonNull String name, String authorId, @NonNull ZonedDateTime datetimeCreated, boolean finished) {
        super(id, synched);
        this.name = name;
        this.authorId = authorId;
        this.datetimeCreated = datetimeCreated;
        this.finished = finished;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public JSONObject toJSONobject() {
        JSONObject jsonObject = null;
        try {
            jsonObject = super.toJSONobject();
            jsonObject.put("name", name);
            jsonObject.put("authorId", authorId);
            jsonObject.put("datetimeCreated", datetimeCreated.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));
            jsonObject.put("finished", finished);
            return jsonObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

    }
}

