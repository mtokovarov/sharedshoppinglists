package com.example.shopping_lists.datalayer.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.example.shopping_lists.datalayer.entities.Category;

import java.util.List;

import io.reactivex.Single;

@Dao
public abstract class CategoryDao {
    @Query("SELECT * FROM Category ORDER BY name")
    public abstract Single<List<Category>> getAll();

    @Query("SELECT * FROM Category WHERE id = :id")
    public abstract Single<Category> getById(String id);
}
