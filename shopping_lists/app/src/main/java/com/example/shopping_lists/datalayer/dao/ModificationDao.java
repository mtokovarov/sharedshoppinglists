package com.example.shopping_lists.datalayer.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.shopping_lists.datalayer.entities.Modification;
import com.example.shopping_lists.datalayer.entities.SynchronizableEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public abstract class ModificationDao {
    @Insert
    public abstract Completable insert(Modification modification);

    @Query("DELETE FROM Modification WHERE tableName = :tableName")
    public abstract Completable deleteAllByTableName(String tableName);

    @Query("DELETE FROM Modification")
    public abstract Completable deleteAll();

    @Query("DELETE FROM Modification WHERE rowId = :id")
    public abstract Completable deleteByRowId(String id);

    @Query("SELECT * FROM Modification")
    public abstract Single<List<Modification>> getAll();

    @Query("DELETE FROM Modification WHERE rowId IN (:ids)")
    public abstract Completable deleteFromModifications(String[] ids);

    public <T extends SynchronizableEntity> Completable deleteFromModifications(List<T> t){
        String[] ids = new String[t.size()];
        for(int i = 0; i < t.size();i++)
            ids[i] = t.get(i).getId();
        return deleteFromModifications(ids);
    }
}
