package com.example.shopping_lists.gui.dialogs;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.datalayer.entities.Category;
import com.example.shopping_lists.datalayer.entities.Item;
import com.example.shopping_lists.datalayer.entities.Product;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class EditItemDialog extends AddItemDialog {
    Item editedItem;

    public EditItemDialog(@NonNull Context context, Item editedItem) {
        super(context, editedItem.getShoppingListId());
        this.editedItem = editedItem;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.productInput.setText(editedItem.getProduct());
        this.quantityInput.setText(editedItem.getQuantity());
        MainActivity.getDatabase().getProductDao().getProductByName(editedItem.getProduct()).subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver<Product>() {
            @Override
            public void onSuccess(Product product) {
                MainActivity.getDatabase().getCategoryDao().getById(product.getCategoryId()).subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver<Category>() {
                    @Override
                    public void onSuccess(Category category) {
                        categoriewWithDeafultAllSpinner.setPositionByCategory(category);
                    }
                    @Override
                    public void onError(Throwable e) {

                    }
                });
            }
            @Override
            public void onError(Throwable e) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void processItem() {
        String product = productInput.getText().toString();
        if (product.contentEquals(""))
            product = productInput.getHint().toString();
        String quantity = quantityInput.getText().toString();
        if (quantity.contentEquals(""))
            quantity = null;
        editedItem.setProduct(product);
        editedItem.setQuantity(quantity);
        editedItem.setFinished(false);
        editedItem.setSynched(false);
        MainActivity.getDatabase().getItemDao().update(editedItem).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
}
