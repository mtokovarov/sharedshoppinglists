package com.example.shopping_lists.datalayer.entities;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.TypeConverters;

import com.example.shopping_lists.utils.DateTimeZoneConverter;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = {
        @ForeignKey(entity = User.class,parentColumns = "userId", childColumns = "userFrom", onDelete = CASCADE),
        @ForeignKey(entity = User.class,parentColumns = "userId", childColumns = "userTo", onDelete = CASCADE)
})
public class Message extends SynchronizableEntity{

    @NonNull
    private String content;
    @NonNull
    @TypeConverters(DateTimeZoneConverter.class)
    private ZonedDateTime datetimeSent;
    @TypeConverters(DateTimeZoneConverter.class)
    private ZonedDateTime datetimeRead;
    @NonNull
    private String userFrom;
    @NonNull
    private String userTo;

    public Message(@NonNull String id, boolean synched, @NonNull String content, @NonNull ZonedDateTime datetimeSent, ZonedDateTime datetimeRead, @NonNull String userFrom, @NonNull String userTo) {
        super(id, synched);
        this.content = content;
        this.datetimeSent = datetimeSent;
        this.datetimeRead = datetimeRead;
        this.userFrom = userFrom;
        this.userTo = userTo;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public JSONObject toJSONobject() {
        try {
            JSONObject jsonObject = super.toJSONobject();
            jsonObject.put("content", content);
            jsonObject.put("datetimeSent", datetimeSent.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));
            jsonObject.put("datetimeRead", datetimeRead.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));
            jsonObject.put("userFrom", userFrom);
            jsonObject.put("userTo", userTo);
            return jsonObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }

    @NonNull
    public ZonedDateTime getDatetimeSent() {
        return datetimeSent;
    }

    public void setDatetimeSent(@NonNull ZonedDateTime datetimeSent) {
        this.datetimeSent = datetimeSent;
    }

    public ZonedDateTime getDatetimeRead() {
        return datetimeRead;
    }

    public void setDatetimeRead(ZonedDateTime datetimeRead) {
        this.datetimeRead = datetimeRead;
    }

    @NonNull
    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(@NonNull String userFrom) {
        this.userFrom = userFrom;
    }

    @NonNull
    public String getUserTo() {
        return userTo;
    }

    public void setUserTo(@NonNull String userTo) {
        this.userTo = userTo;
    }

    public boolean isSynched() {
        return synched;
    }

    public void setSynched(boolean synched) {
        this.synched = synched;
    }


}


