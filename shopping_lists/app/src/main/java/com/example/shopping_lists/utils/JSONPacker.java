package com.example.shopping_lists.utils;

import com.example.shopping_lists.datalayer.entities.SynchronizableEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class JSONPacker {
    public static <T extends SynchronizableEntity> JSONObject packIntoJSONObject(List<T> entities, String tableName){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("tableName", tableName);
            JSONArray jsonArray = new JSONArray();
            for (T entity:entities){
                jsonArray.put(entity.toJSONobject());
            }
            jsonObject.put("data", jsonArray);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return jsonObject;
    }

    public static JSONObject packIntoJSONObject(String[] strings, String tableName){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("tableName", tableName);
            JSONArray jsonArray = new JSONArray();
            for (String str:strings){
                jsonArray.put(str);
            }
            jsonObject.put("data", jsonArray);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return jsonObject;
    }

}
