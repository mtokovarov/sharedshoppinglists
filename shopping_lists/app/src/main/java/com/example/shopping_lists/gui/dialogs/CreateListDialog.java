package com.example.shopping_lists.gui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.InputType;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.RequiresApi;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.datalayer.entities.ShoppingList;

import java.time.ZonedDateTime;
import java.util.UUID;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class CreateListDialog {

    @RequiresApi(api = Build.VERSION_CODES.O)
    public CreateListDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Provide list name");
        final EditText input = new EditText(context);
        input.setFocusedByDefault(true);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String listName = input.getText().toString();
                if (listName == "")
                    return;
                String id = UUID.randomUUID().toString();
                ShoppingList shoppingList = new ShoppingList(id, false,listName, MainActivity.getOwner().getUserId(),ZonedDateTime.now(),false);
                MainActivity.getDatabase().getShoppingListDao().insert(shoppingList).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
        input.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
