package com.example.shopping_lists.datalayer.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Modification {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String tableName;
    @NonNull
    private String rowId;
    @NonNull
    private String modificationType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getModificationType() {
        return modificationType;
    }

    public void setModificationType(String modificationType) {
        this.modificationType = modificationType;
    }

    public Modification(@NonNull String tableName, @NonNull String rowId, @NonNull String modificationType) {
        this.tableName = tableName;
        this.rowId = rowId;
        this.modificationType = modificationType;
    }
}
