package com.example.shopping_lists.gui.spinners;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.datalayer.entities.Category;

import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CategoriewWithDeafultAllSpinner {
    private Spinner spinner;
    private List<Category> categories;

    public CategoriewWithDeafultAllSpinner(Spinner spinner, Context context) {
        this.spinner = spinner;
        MainActivity.getDatabase().getCategoryDao().getAll().subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver<List<Category>>() {
            @Override
            public void onSuccess(List<Category> categories) {
                CategoriewWithDeafultAllSpinner.this.categories = categories;
                String[] categoryNames = new String[categories.size()+1];
                categoryNames[0] = "All categories";
                for (int i = 0; i < categories.size(); i++)
                    categoryNames[i+1] = categories.get(i).getName();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, categoryNames);
                spinner.setAdapter(adapter);
            }
            @Override
            public void onError(Throwable e) {

            }
        });
    }

    public Category getSelectedCategoryOrNull(){
        if (spinner.getSelectedItemPosition()==0)
            return null;
        return categories.get(spinner.getSelectedItemPosition()-1);
    }

    public Spinner getSpinner() {
        return spinner;
    }
    public void setPositionByCategory(Category category){
        for (int i = 0; i < categories.size(); i++)
            if (categories.get(i).getName().contentEquals(category.getName())) {
                spinner.setSelection(i+1);
                return;
            }
    }
}
