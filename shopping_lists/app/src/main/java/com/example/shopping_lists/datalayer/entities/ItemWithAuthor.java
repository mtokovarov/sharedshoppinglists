package com.example.shopping_lists.datalayer.entities;

import androidx.room.Embedded;
import androidx.room.Relation;

public class ItemWithAuthor {
    @Embedded
    public Item item;
    @Relation(parentColumn = "authorId",
            entityColumn = "userId"
    )
    public User author;
}
