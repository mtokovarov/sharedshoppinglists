package com.example.shopping_lists.datalayer.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"purchaseListId", "userId"},
        foreignKeys = {
        @ForeignKey(entity = User.class, parentColumns = "userId", childColumns = "userId", onDelete = CASCADE),
        @ForeignKey(entity = ShoppingList.class, parentColumns = "id", childColumns = "purchaseListId", onDelete = CASCADE)
})
public class UserListCrossRef {
    @NonNull
    private String userId;
    @NonNull
    private String purchaseListId;
    private boolean synched;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPurchaseListId() {
        return this.purchaseListId;
    }

    public void setPurchaseListId(String purchaseListId) {
        this.purchaseListId = purchaseListId;
    }

    public boolean isSynched() {
        return synched;
    }

    public void setSynched(boolean synched) {
        this.synched = synched;
    }


}
