package com.example.shopping_lists.gui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopping_lists.R;
import com.example.shopping_lists.gui.adapters.ShoppingListItemsAdapter;
import com.example.shopping_lists.gui.dialogs.AddItemDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ItemsFragment extends Fragment {
    private String selectedShoppingListId;
    private RecyclerView itemsRecyclerView;
    private ShoppingListItemsAdapter shoppingListItemsAdapter;



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shopping_list_items, container, false);

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectedShoppingListId = ItemsFragmentArgs.fromBundle(getArguments()).getSelectedShoppingListId();
        initRecyclerView();
        initAddItemButton();
        initToMessageButton();
    }

    private void initAddItemButton(){
        FloatingActionButton fab = getActivity().findViewById(R.id.fabFragmentItems);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AddItemDialog(getContext(), selectedShoppingListId).show();
            }
        });
    }

    private void initRecyclerView(){
        itemsRecyclerView = getActivity().findViewById(R.id.itemRecyclerView);
        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        shoppingListItemsAdapter = new ShoppingListItemsAdapter(getContext(), selectedShoppingListId);
        itemsRecyclerView.setAdapter(shoppingListItemsAdapter);
        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemsRecyclerView.addItemDecoration(decoration);
    }

    private void initToMessageButton(){
        getActivity().findViewById(R.id.shoppingListItemsToMessagesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(ItemsFragment.this)
                        .navigate(R.id.action_SecondFragment_to_messageFragment2);
            }
        });
    }

}
