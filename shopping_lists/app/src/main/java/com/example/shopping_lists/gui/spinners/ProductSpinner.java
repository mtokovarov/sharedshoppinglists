package com.example.shopping_lists.gui.spinners;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.datalayer.entities.Product;

import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ProductSpinner  <T extends View> {
    private Spinner spinner;
    private List<Product> products;
    ArrayAdapter<String> adapter;
    Context context;

    public ProductSpinner (Spinner spinner, Context context) {
        this.spinner = spinner;
        this.context = context;
        MainActivity.getDatabase().getProductDao().getAllProducts().subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver<List<Product>>() {
            @Override
            public void onSuccess(List<Product> products) {
                ProductSpinner.this.products = products;
                setAdapter(products);
            }
            @Override
            public void onError(Throwable e) {

            }
        });
    }

    public void setAdapter(List<Product> products){
        String[] productNames = new String[products.size()];
        for (int i = 0; i < products.size();i++)
            productNames[i] = products.get(i).getName();
        adapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, productNames);
        this.products = products;
        spinner.setAdapter(adapter);
    }

    public Product getSelectedProduct(){
        return products.get(spinner.getSelectedItemPosition());
    }

    public Spinner getSpinner() {
        return spinner;
    }


    public void setPositionByProduct(Product product){
        for (int i = 0; i < products.size(); i++)
            if (products.get(i).getName().contentEquals(product.getName())) {
                spinner.setSelection(i);
                return;
            }
    }
}
