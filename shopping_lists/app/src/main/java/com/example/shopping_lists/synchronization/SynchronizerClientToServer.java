package com.example.shopping_lists.synchronization;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.Dao;
import androidx.room.Transaction;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.datalayer.dao.SynchronizableEntityDao;
import com.example.shopping_lists.datalayer.entities.SynchronizableEntity;
import com.example.shopping_lists.utils.JSONPacker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

@Dao
public abstract class SynchronizerClientToServer {
    RequestQueue requestQueue;
    private final static String serverURL = "http://10.0.2.2:8080/synchronization";
    Context context;
    Disposable shoppingListsAndItemsDisposable;

    public SynchronizerClientToServer(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
    }

    public <Tdao extends SynchronizableEntityDao, T extends SynchronizableEntity> void synchronizeIsnerted(Tdao tdao){
        tdao.getInserted().subscribeOn(Schedulers.single()).subscribeWith(new DisposableSingleObserver() {
            @Override
            public void onSuccess(Object o) {
                List<T> entities = (List<T>)o;
                if (entities.size()==0)
                    return;
                JSONObject jsonObject = JSONPacker.packIntoJSONObject(entities, tdao.getTableName());
                try {
                    jsonObject.put("user_id", MainActivity.getOwner().getUserId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                MyStringRequest myStringRequest = new MyStringRequest(serverURL + "/insert", StringRequest.Method.POST, jsonObject) {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponseReceived(String respons) {
                        if (respons.contentEquals("Ok")) {
                            MainActivity.getDatabase().getModificationDao().deleteFromModifications(entities).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                                @Override
                                public void onComplete() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                            tdao.setSynchedTrue(entities).subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver() {
                                @Override
                                public void onSuccess(Object o) {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                        }
                    }
                };
                requestQueue.add(myStringRequest.getRequest());
            }

            @Override
            public void onError(Throwable e) {
            }
        });

    }

    public <Tdao extends SynchronizableEntityDao, T extends SynchronizableEntity> void synchronizeUpdated(Tdao tdao){
        tdao.getUpdated().subscribeOn(Schedulers.single()).subscribeWith(new DisposableSingleObserver() {
            @Override
            public void onSuccess(Object o) {
                List<T> entities = (List<T>)o;
                if (entities.size()==0)
                    return;
                JSONObject jsonObject = JSONPacker.packIntoJSONObject(entities, tdao.getTableName());
                try {
                    jsonObject.put("user_id", MainActivity.getOwner().getUserId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                MyStringRequest myStringRequest = new MyStringRequest(serverURL + "/update", StringRequest.Method.POST, jsonObject) {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponseReceived(String respons) {
                        if (respons.contentEquals("Ok")) {
                            MainActivity.getDatabase().getModificationDao().deleteFromModifications(entities).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                                @Override
                                public void onComplete() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                            tdao.setSynchedTrue(entities).subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver() {
                                @Override
                                public void onSuccess(Object o) {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                        }
                    }
                };
                requestQueue.add(myStringRequest.getRequest());
            }

            @Override
            public void onError(Throwable e) {
            }
        });
    }

    public <Tdao extends SynchronizableEntityDao> void synchronizeDeleted(Tdao tdao){
        tdao.getUpdated().subscribeOn(Schedulers.single()).subscribeWith(new DisposableSingleObserver() {
            @Override
            public void onSuccess(Object o) {
                String[] ids = (String[]) o;
                if (ids.length==0)
                    return;
                JSONObject jsonObject = JSONPacker.packIntoJSONObject(ids, tdao.getTableName());
                try {
                    jsonObject.put("user_id", MainActivity.getOwner().getUserId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                MyStringRequest myStringRequest = new MyStringRequest(serverURL + "/delete", StringRequest.Method.POST, jsonObject) {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponseReceived(String respons) {
                        if (respons.contentEquals("Ok")) {
                            MainActivity.getDatabase().getModificationDao().deleteFromModifications(ids).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                                @Override
                                public void onComplete() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                            tdao.setSynchedTrue(ids).subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver() {
                                @Override
                                public void onSuccess(Object o) {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                        }
                    }
                };
                requestQueue.add(myStringRequest.getRequest());
            }

            @Override
            public void onError(Throwable e) {
            }
        });
    }


    @Transaction
    public void synchronizeShoppingListsAndItems(){
        synchronizeIsnerted(MainActivity.getDatabase().getShoppingListDao());
        synchronizeUpdated(MainActivity.getDatabase().getShoppingListDao());
        synchronizeDeleted(MainActivity.getDatabase().getShoppingListDao());
        synchronizeIsnerted(MainActivity.getDatabase().getItemDao());
        synchronizeUpdated(MainActivity.getDatabase().getItemDao());
        synchronizeDeleted(MainActivity.getDatabase().getItemDao());
    }

    public void initSyncShoppingListsAndItems(){
        if (shoppingListsAndItemsDisposable != null)
            shoppingListsAndItemsDisposable.dispose();
        shoppingListsAndItemsDisposable =
        Schedulers.io().schedulePeriodicallyDirect(new Runnable() {
            @Override
            public void run() {
                synchronizeShoppingListsAndItems();
            }
        }, 0, 20, TimeUnit.SECONDS);
    }

    public void synchronizeMessages(){
        synchronizeIsnerted(MainActivity.getDatabase().getMessageDao());
        synchronizeUpdated(MainActivity.getDatabase().getMessageDao());
        synchronizeDeleted(MainActivity.getDatabase().getMessageDao());
    }



}
