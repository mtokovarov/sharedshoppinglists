package com.example.shopping_lists.gui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.datalayer.entities.Category;
import com.example.shopping_lists.datalayer.entities.Item;
import com.example.shopping_lists.datalayer.entities.Product;
import com.example.shopping_lists.gui.spinners.CategoriewWithDeafultAllSpinner;
import com.example.shopping_lists.gui.spinners.ProductSpinner;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import io.reactivex.Single;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class AddItemDialog extends Dialog {
    protected ProductSpinner productSpinner;
    protected CategoriewWithDeafultAllSpinner categoriewWithDeafultAllSpinner;
    protected EditText productInput;
    protected EditText quantityInput;
    private Button okButton;
    private Button cancelButton;
    private Context context;
    private String shoppingListId;

    public AddItemDialog(@NonNull Context context, String shoppingListId) {
        super(context);
        this.context = context;
        this.shoppingListId = shoppingListId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_item_dialog);
        quantityInput = findViewById(R.id.itemInputQuantity);
        productInput = findViewById(R.id.itemInputProduct);
        initProductSpinner();
        initCategorySpinner();
        initOkButton();
        initCancelButton();
    }

    private void initProductSpinner(){
        productSpinner = new ProductSpinner(findViewById(R.id.productSpinner), getContext());
        this.productSpinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AddItemDialog.this.productInput.setText("");
                AddItemDialog.this.productInput.setHint(productSpinner.getSelectedProduct().getName());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCategorySpinner(){
        categoriewWithDeafultAllSpinner = new CategoriewWithDeafultAllSpinner(findViewById(R.id.categoriesSpinner), context);
        categoriewWithDeafultAllSpinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = categoriewWithDeafultAllSpinner.getSelectedCategoryOrNull();
                Single<List<Product>> singleListProducts;
                if (selectedCategory == null)
                    singleListProducts = MainActivity.getDatabase().getProductDao().getAllProducts();
                else
                    singleListProducts = MainActivity.getDatabase().getProductDao().getProductsByCategoryId(selectedCategory.getId());
                singleListProducts.subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver<List<Product>>() {
                    @Override
                    public void onSuccess(List<Product> products) {
                        productSpinner.setAdapter(products);
                    }
                    @Override
                    public void onError(Throwable e) {

                    }
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initOkButton(){
        this.okButton = findViewById(R.id.newItemOkButton);
        this.okButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                processItem();
                dismiss();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void processItem(){
        String productName = productInput.getText().toString();
        if (productName.contentEquals(""))
            productName = productInput.getHint().toString();
        String quantity = quantityInput.getText().toString();
        String id = UUID.randomUUID().toString();
        String authorId = MainActivity.getOwner().getUserId();
        if (quantity == "")
            quantity = null;
        Item newItem = new Item(id,false,productName,quantity, ZonedDateTime.now(),false, authorId,this.shoppingListId);
        MainActivity.getDatabase().getItemDao().insert(newItem).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    private void initCancelButton(){
        cancelButton = findViewById(R.id.newItemCancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
