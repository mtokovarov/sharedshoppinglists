package com.example.shopping_lists.datalayer;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.shopping_lists.datalayer.dao.CategoryDao;
import com.example.shopping_lists.datalayer.dao.ItemDao;
import com.example.shopping_lists.datalayer.dao.MessageDao;
import com.example.shopping_lists.datalayer.dao.ModificationDao;
import com.example.shopping_lists.datalayer.dao.ProductDao;
import com.example.shopping_lists.datalayer.dao.ShoppingListDao;
import com.example.shopping_lists.datalayer.dao.UserDao;
import com.example.shopping_lists.datalayer.entities.Category;
import com.example.shopping_lists.datalayer.entities.Item;
import com.example.shopping_lists.datalayer.entities.Message;
import com.example.shopping_lists.datalayer.entities.Modification;
import com.example.shopping_lists.datalayer.entities.Product;
import com.example.shopping_lists.datalayer.entities.ShoppingList;
import com.example.shopping_lists.datalayer.entities.User;
import com.example.shopping_lists.datalayer.entities.UserListCrossRef;

@Database(version = 1, entities = {Category.class, Product.class, User.class, Message.class,
        ShoppingList.class, Item.class, UserListCrossRef.class, Modification.class})
public abstract class ShoppingListsDatabase extends RoomDatabase {
    public abstract UserDao getUserDao();
    public abstract ProductDao getProductDao();
    public abstract ModificationDao getModificationDao();
    public abstract CategoryDao getCategoryDao();
    public abstract ShoppingListDao getShoppingListDao();
    public abstract ItemDao getItemDao();
    public abstract MessageDao getMessageDao();
}
