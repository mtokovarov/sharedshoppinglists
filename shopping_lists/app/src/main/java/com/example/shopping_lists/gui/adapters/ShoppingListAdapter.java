package com.example.shopping_lists.gui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.datalayer.entities.ShoppingList;
import com.example.shopping_lists.datalayer.entities.ShoppingListWithAuthor;
import com.example.shopping_lists.datalayer.entities.User;
import com.example.shopping_lists.gui.fragments.ShoppingListsFragment;
import com.example.shopping_lists.gui.fragments.ShoppingListsFragmentDirections;
import com.example.shopping_lists.gui.spinners.UsersWithDefaultAllSpinner;

import java.time.format.DateTimeFormatter;
import java.util.List;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class ShoppingListAdapter extends Adapter<ShoppingListAdapter.ShoppingListHolder> {
    private List<ShoppingListWithAuthor> shoppingListsWithAuthors;
    private LiveData<List<ShoppingListWithAuthor>> shoppingListsWithAuthorsLiveData;
    Context context;
    ShoppingListsFragment shoppingListsFragment;

    public ShoppingListAdapter(Context context, ShoppingListsFragment shoppingListsFragment) {
        this.context = context;
        this.shoppingListsFragment = shoppingListsFragment;
        initUserSpinner();
    }

    @NonNull
    @Override
    public ShoppingListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_list_adapter, parent, false);
        return new ShoppingListHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ShoppingListHolder holder, int position) {
        initHolderElements(holder, position);
        setDeleteButton(holder, position);
        initCheckBox(holder, position);
        initListOpening(holder, position);
    }

    @Override
    public int getItemCount() {
        if (shoppingListsWithAuthors == null)
            return 0;
        return shoppingListsWithAuthors.size();
    }

    private void initUserSpinner(){
        UsersWithDefaultAllSpinner us = new UsersWithDefaultAllSpinner(context, R.id.shoppingListUsersSpinner);
        us.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = us.getSelectedUserOrNull();
                if (shoppingListsWithAuthorsLiveData != null)
                    shoppingListsWithAuthorsLiveData.removeObservers((LifecycleOwner) context);
                if (selectedUser == null){
                    shoppingListsWithAuthorsLiveData = MainActivity.getDatabase().getShoppingListDao().getShoppingListsWithAuthorsOrderedByDate();
                }
                else{
                    shoppingListsWithAuthorsLiveData = MainActivity.getDatabase().getShoppingListDao().
                            getShoppingListsWithAuthorsOrderedByDateOfSelectedAuthor(selectedUser.getUserId());
                }
                shoppingListsWithAuthorsLiveData.observe((LifecycleOwner) context, new Observer<List<ShoppingListWithAuthor>>() {
                    @Override
                    public void onChanged(List<ShoppingListWithAuthor> shoppingListWithAuthors) {
                        shoppingListsWithAuthors = shoppingListWithAuthors;
                        notifyDataSetChanged();
                    }
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initHolderElements(@NonNull ShoppingListHolder holder, int position) {
        ShoppingListWithAuthor list = shoppingListsWithAuthors.get(position);
        holder.authorLastName.setText(list.author.getLastName());
        holder.authorFirstName.setText(list.author.getFirstName());
        holder.finishedCB.setChecked(list.shoppingList.isFinished());
        holder.shoppingListName.setText(list.shoppingList.getName());
        holder.date.setText(list.shoppingList.getDatetimeCreated().format(DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss")));
        if (list.shoppingList.isSynched())
            holder.isSynchedList.setImageResource(android.R.drawable.stat_sys_upload_done);
        else
            holder.isSynchedList.setImageResource(android.R.drawable.ic_popup_sync);
    }

    private void setDeleteButton(ShoppingListHolder holder, final int position){
        ShoppingListWithAuthor list = shoppingListsWithAuthors.get(position);
        if (!list.shoppingList.getAuthorId().contentEquals(MainActivity.getOwner().getUserId())){
            holder.deleteListButton.setVisibility(View.INVISIBLE);
            return;
        }
        holder.deleteListButton.setVisibility(View.VISIBLE);
        holder.deleteListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Confirm");
                builder.setMessage("Do you really want to delete the whole list? \nWarning: all the items of the list will be removed as well!");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void onClick(DialogInterface dialog, int which) {
                        ShoppingList slToDelete = list.shoppingList;
                        MainActivity.getDatabase().getShoppingListDao().delete(slToDelete).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {}

                            @Override
                            public void onError(Throwable e) {}
                        });
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void initCheckBox(ShoppingListHolder holder, final int position){
        holder.finishedCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ShoppingList list = shoppingListsWithAuthors.get(position).shoppingList;
                if (isChecked == list.isFinished())
                    return;
                list.setFinished(isChecked);
                list.setSynched(false);
                MainActivity.getDatabase().getShoppingListDao().update(list).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
            }
        });
    }

    private void initListOpening(ShoppingListHolder holder, final int position){
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShoppingList selectedShoppingList = shoppingListsWithAuthors.get(position).shoppingList;
                NavHostFragment.findNavController(shoppingListsFragment).
                        navigate(ShoppingListsFragmentDirections.actionFirstFragmentToSecondFragment(selectedShoppingList.getId()));
            }
        });

    }

    public class ShoppingListHolder extends RecyclerView.ViewHolder{
        public CheckBox finishedCB;
        public TextView shoppingListName;
        public TextView authorFirstName;
        public TextView authorLastName;
        public TextView date;
        public ImageView isSynchedList;
        public Button deleteListButton;
        public ShoppingListHolder(@NonNull View itemView) {
            super(itemView);
            finishedCB = itemView.findViewById(R.id.shoppingListFinishedCheckBox);
            shoppingListName = itemView.findViewById(R.id.shoppingListName);
            authorFirstName = itemView.findViewById(R.id.shoppingListAuthorFirstName);
            authorLastName = itemView.findViewById(R.id.shoppingListAuthorLastName);
            date = itemView.findViewById(R.id.shoppingListDateTimeCreated);
            isSynchedList = itemView.findViewById(R.id.isSynchedList);
            deleteListButton = itemView.findViewById(R.id.deleteShoppingListButton);
        }
    }

    @Override
    public void onViewRecycled(@NonNull ShoppingListHolder holder) {
        holder.finishedCB.setOnCheckedChangeListener(null);
        super.onViewRecycled(holder);
    }
}
