package com.example.shopping_lists.gui.fragments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.gui.dialogs.CreateListDialog;
import com.example.shopping_lists.gui.adapters.ShoppingListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ShoppingListsFragment extends Fragment {
    ShoppingListAdapter shoppingListAdapter;

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sharing_unsharing, menu);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shopping_lists, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initShoppingListRecyclerView();
        initAddList();
        initToMessageButton();
    }

    private void initShoppingListRecyclerView(){
        RecyclerView shoppingListRecyclerView = ((MainActivity) getContext()).findViewById(R.id.shoppingListRecyclerView);
        shoppingListRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        shoppingListAdapter = new ShoppingListAdapter(getContext(), this);
        shoppingListRecyclerView.setAdapter(shoppingListAdapter);
        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        shoppingListRecyclerView.addItemDecoration(decoration);
    }

    private void initAddList(){
        FloatingActionButton fab = ((Activity)this.getContext()).findViewById(R.id.fabFragmentLists);
        fab.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                new CreateListDialog(getContext());
            }
        });
    }

    public void navigateToSecondFragment(){
        NavHostFragment.findNavController(ShoppingListsFragment.this)
                .navigate(R.id.action_FirstFragment_to_SecondFragment);
    }

    private void initToMessageButton(){
        getActivity().findViewById(R.id.shoppingListsToMessagesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(ShoppingListsFragment.this)
                        .navigate(R.id.action_FirstFragment_to_messageFragment2);
            }
        });
    }

}
