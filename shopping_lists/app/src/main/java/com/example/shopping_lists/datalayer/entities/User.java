package com.example.shopping_lists.datalayer.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.example.shopping_lists.utils.DateTimeZoneConverter;

import java.time.ZonedDateTime;

@Entity
public class User {
    @PrimaryKey
    @NonNull
    private String userId;
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    @NonNull
    @TypeConverters(DateTimeZoneConverter.class)
    private ZonedDateTime birthDate;
    @NonNull
    private boolean isOwner;

    public User(@NonNull String userId, @NonNull String firstName, @NonNull String lastName, @NonNull ZonedDateTime birthDate, boolean isOwner) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.isOwner = isOwner;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NonNull String firstName) {
        this.firstName = firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@NonNull String lastName) {
        this.lastName = lastName;
    }

    @NonNull
    public ZonedDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(@NonNull ZonedDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean owner) {
        isOwner = owner;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }
}
