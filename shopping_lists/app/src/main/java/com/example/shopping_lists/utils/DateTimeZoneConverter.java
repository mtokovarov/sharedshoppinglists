package com.example.shopping_lists.utils;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.TypeConverter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@RequiresApi(api = Build.VERSION_CODES.O)
public class DateTimeZoneConverter {
    private DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

    @TypeConverter
    public String dateToString(ZonedDateTime d){
        if (d==null)
            return null;

        return d.format(formatter);
    }
    @TypeConverter
    public ZonedDateTime stringToDate(String date){
        if(date == null)
            return null;
        return ZonedDateTime.parse(date, formatter);
    }
}
