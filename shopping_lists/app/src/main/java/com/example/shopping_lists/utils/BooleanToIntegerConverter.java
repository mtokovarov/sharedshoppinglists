package com.example.shopping_lists.utils;

import androidx.room.TypeConverter;

public class BooleanToIntegerConverter {
    @TypeConverter
    public boolean intToBool(int val){
        return val==1;
    }
    @TypeConverter
    public int boolToInt(boolean val){
        if (val)
            return 1;
        return 0;
    }
}
