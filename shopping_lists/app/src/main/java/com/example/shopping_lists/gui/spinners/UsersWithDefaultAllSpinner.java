package com.example.shopping_lists.gui.spinners;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.datalayer.entities.User;

import java.util.List;

public class UsersWithDefaultAllSpinner {
    private Spinner spinner;
    private List<User> users;

    public UsersWithDefaultAllSpinner(Context c, int resourceId) {
        spinner = ((AppCompatActivity)c).findViewById(resourceId);
        MainActivity.getDatabase().getUserDao().getAllUsers().observe((LifecycleOwner) c, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> changed_users) {
                users = changed_users;
                String[] labels = new String[changed_users.size()+1];
                labels[0] = "All users";
                for (int i = 0; i < changed_users.size(); i++)
                    labels[i+1] = users.get(i).getFirstName() + " " + users.get(i).getLastName();
                ArrayAdapter<String> usersAdapter = new ArrayAdapter<String>(c, R.layout.support_simple_spinner_dropdown_item, labels);
                spinner.setAdapter(usersAdapter);
            }
        });
    }

    public User getSelectedUserOrNull(){
        int selectedPosition = spinner.getSelectedItemPosition();
        if (selectedPosition == 0)
            return null;
        return users.get(selectedPosition-1);
    }

    public Spinner getSpinner() {
        return spinner;
    }
}
