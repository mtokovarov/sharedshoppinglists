package com.example.shopping_lists.synchronization;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

public abstract class MyStringRequest {
    StringRequest stringRequest;
    public MyStringRequest(String url, int method, JSONObject jsonBody) {
        Response.Listener listener = new Response.Listener(){
            @Override
            public void onResponse(Object response) {
                onResponseReceived((String)response);
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
        stringRequest = new StringRequest(method, url, listener, errorListener){
            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonBody.toString().getBytes();
            }
        };
    }

    public StringRequest getRequest(){
        return stringRequest;
    }

    public abstract void onResponseReceived(String response);
}
