package com.example.shopping_lists.datalayer.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.example.shopping_lists.datalayer.entities.ShoppingList;
import com.example.shopping_lists.datalayer.entities.ShoppingListWithAuthor;

import java.util.List;

import io.reactivex.Single;

@Dao
public abstract class ShoppingListDao extends SynchronizableEntityDao<ShoppingList> {
    @Query("SELECT * FROM ShoppingList sl LEFT JOIN User u ON u.userId=sl.authorId ORDER BY strftime('%s',sl.datetimeCreated)")
    public abstract LiveData<List<ShoppingListWithAuthor>> getShoppingListsWithAuthorsOrderedByDate();

    @Query("SELECT * FROM ShoppingList sl INNER JOIN User u ON u.userId=sl.authorId WHERE sl.authorId = :authId ORDER BY strftime('%s',sl.datetimeCreated)")
    public abstract LiveData<List<ShoppingListWithAuthor>> getShoppingListsWithAuthorsOrderedByDateOfSelectedAuthor(String authId);

    @Query("SELECT * FROM shoppingList WHERE id=:id")
    public abstract Single<ShoppingList> getShoppingListById(String id);
}
