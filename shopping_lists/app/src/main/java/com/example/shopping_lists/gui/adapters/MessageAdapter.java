package com.example.shopping_lists.gui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.datalayer.entities.Message;
import com.example.shopping_lists.datalayer.entities.User;
import com.example.shopping_lists.gui.spinners.UsersSpinner;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {
    List<Message> messages;
    LiveData<List<Message>> messagesLiveData;
    UsersSpinner usersSpinner;
    Context context;
    EditText messageInput;
    RecyclerView recyclerView;



    public MessageAdapter(Context context, RecyclerView recyclerView) {
        this.context = context;
        messageInput = ((Activity)context).findViewById(R.id.inputMessageEditText);
        this.recyclerView = recyclerView;
        initUserSpinner();
        initSendButton();

    }

    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_adapter, parent, false);
        return new MessageHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, int position) {
        Message message = messages.get(position);
        holder.contentTextView.setText(message.getContent());
        ZonedDateTime read = message.getDatetimeRead();
        if (read == null)
            holder.readStatusTextView.setText("Not read yet");
        else
            holder.readStatusTextView.setText(read.format(DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss")));
        if (message.isSynched())
            holder.synchedStatusImageView.setImageResource(android.R.drawable.ic_menu_send);
        else
            holder.synchedStatusImageView.setImageResource(android.R.drawable.ic_popup_sync);
        if (message.getUserFrom().contentEquals(MainActivity.getOwner().getUserId())) {
            holder.contentTextView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            holder.contentTextView.setTextColor(Color.BLUE);
        }
        else {
            holder.contentTextView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        }
        initDeleteMessage(holder, position);

    }

    @Override
    public int getItemCount() {
        if (messages == null)
            return 0;
        return messages.size();
    }

    public class MessageHolder extends RecyclerView.ViewHolder{
        public TextView contentTextView;
        public TextView readStatusTextView;
        public ImageView synchedStatusImageView;
        public Button deleteMessage;

        public MessageHolder(@NonNull View itemView) {
            super(itemView);
            contentTextView = itemView.findViewById(R.id.messageContentTextView);
            readStatusTextView = itemView.findViewById(R.id.readStatusTextView);
            synchedStatusImageView = itemView.findViewById(R.id.isSynchedItemImageView);
            deleteMessage = itemView.findViewById(R.id.deleteMessageButton);
        }
    }

    private void initUserSpinner(){
        usersSpinner = new UsersSpinner(context, R.id.messageUserSpinner);
        usersSpinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser =  usersSpinner.getSelectedUser();
                if (messagesLiveData != null)
                    messagesLiveData.removeObservers((LifecycleOwner) context);
                messagesLiveData = MainActivity.getDatabase().getMessageDao().getMessagesToAndFromUserIdFromOlders(selectedUser.getUserId());
                messagesLiveData.observe((LifecycleOwner) context, new Observer<List<Message>>() {
                    @Override
                    public void onChanged(List<Message> messages) {
                        MessageAdapter.this.messages = messages;
                        MessageAdapter.this.notifyDataSetChanged();
                        recyclerView.getLayoutManager().scrollToPosition(messages.size()-1);
                    }
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initSendButton(){
        ((Activity)context).findViewById(R.id.sendMessageButton).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if(messageInput.getText().toString().contentEquals(""))
                    return;
                String content = messageInput.getText().toString();
                ZonedDateTime sentTime = ZonedDateTime.now();
                String userTo = usersSpinner.getSelectedUser().getUserId();
                String userFrom = MainActivity.getOwner().getUserId();
                String id = UUID.randomUUID().toString();
                Message newMessage = new Message(id, false, content, sentTime, null,userFrom, userTo);
                MainActivity.getDatabase().getMessageDao().insert(newMessage).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete(){}

                    @Override
                    public void onError(Throwable e) {}
                });
                messageInput.setText("");
            }
        });
    }

    private void initDeleteMessage(MessageHolder holder, int position){
        Message m = messages.get(position);
        if (!m.getUserFrom().contentEquals(MainActivity.getOwner().getUserId())){
            holder.deleteMessage.setVisibility(View.INVISIBLE);
            return;
        }
        holder.deleteMessage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Confirm");
                builder.setMessage("Do you really want to delete the message?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.getDatabase().getMessageDao().delete(m).subscribeOn(Schedulers.io()).subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {}

                            @Override
                            public void onError(Throwable e) {}
                        });
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

}
