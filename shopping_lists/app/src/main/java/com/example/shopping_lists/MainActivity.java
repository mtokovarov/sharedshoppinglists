package com.example.shopping_lists;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.example.shopping_lists.datalayer.ShoppingListsDatabase;
import com.example.shopping_lists.datalayer.entities.User;
import com.example.shopping_lists.gui.spinners.UsersWithDefaultAllSpinner;

import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static ShoppingListsDatabase pd = null;
    public static ShoppingListsDatabase getDatabase(){
        return pd;
    }
    private UsersWithDefaultAllSpinner usersWithDefaultAllSpinner;
    private static User owner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init_Database();
        init_owner();
    }

    private void init_Database(){
        pd = Room.databaseBuilder(this, ShoppingListsDatabase.class,"PurchaseLists.db").
                createFromAsset("database/purchaseLists.db").build();
    }

    public UsersWithDefaultAllSpinner getUsersWithDefaultAllSpinner() {
        return usersWithDefaultAllSpinner;
    }

    private void init_owner() {
        pd.getUserDao().getOwner().subscribeOn(Schedulers.io()).subscribeWith(new DisposableSingleObserver<User>() {
            @Override
            public void onSuccess(User user) {owner = user;}

            @Override
            public void onError(Throwable e) throws RuntimeException {
                System.out.println(e);
                    throw new RuntimeException("No owner found!");
            }
        });
    }

    public static User getOwner() {
        return owner;
    }


}
