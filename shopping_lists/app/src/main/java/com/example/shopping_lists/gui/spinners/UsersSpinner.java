package com.example.shopping_lists.gui.spinners;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.example.shopping_lists.MainActivity;
import com.example.shopping_lists.R;
import com.example.shopping_lists.datalayer.entities.User;

import java.util.List;

public class UsersSpinner {
    private Spinner spinner;
    private List<User> users;

    public UsersSpinner(Context c, int resourceId) {
        spinner = ((AppCompatActivity)c).findViewById(resourceId);
        MainActivity.getDatabase().getUserDao().getUsersWithoutOwner().observe((LifecycleOwner) c, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> changed_users) {
                users = changed_users;
                String[] labels = new String[changed_users.size()];
                for (int i = 0; i < changed_users.size(); i++)
                    labels[i] = users.get(i).getFirstName() + " " + users.get(i).getLastName();
                ArrayAdapter<String> usersAdapter = new ArrayAdapter<String>(c, R.layout.support_simple_spinner_dropdown_item, labels);
                spinner.setAdapter(usersAdapter);
            }
        });
    }

    public User getSelectedUser(){
        return users.get(spinner.getSelectedItemPosition());
    }

    public Spinner getSpinner() {
        return spinner;
    }
}
