package com.example.shopping_lists.datalayer.entities;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.TypeConverters;

import com.example.shopping_lists.utils.BooleanToIntegerConverter;
import com.example.shopping_lists.utils.DateTimeZoneConverter;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.SET_NULL;

@Entity(foreignKeys = {
        @ForeignKey(entity = User.class, parentColumns = "userId", childColumns = "authorId", onDelete = SET_NULL),
        @ForeignKey(entity = ShoppingList.class, parentColumns = "id", childColumns = "shoppingListId", onDelete = CASCADE)
})
public class Item extends SynchronizableEntity {
    @NonNull
    private String product;
    private String quantity;
    @NonNull
    @TypeConverters(DateTimeZoneConverter.class)
    private ZonedDateTime datetimeCreated;
    @NonNull
    @TypeConverters(BooleanToIntegerConverter.class)
    private boolean finished;
    private String authorId;
    @NonNull
    private String shoppingListId;


    public Item(@NonNull String id, boolean synched, @NonNull String product, String quantity, @NonNull ZonedDateTime datetimeCreated, boolean finished, String authorId, @NonNull String shoppingListId) {
        super(id, synched);
        this.product = product;
        this.quantity = quantity;
        this.datetimeCreated = datetimeCreated;
        this.finished = finished;
        this.authorId = authorId;
        this.shoppingListId = shoppingListId;
    }

    @NonNull
    public String getProduct() {
        return product;
    }

    public void setProduct(@NonNull String product) {
        this.product = product;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @NonNull
    public ZonedDateTime getDatetimeCreated() {
        return datetimeCreated;
    }

    public void setDatetimeCreated(@NonNull ZonedDateTime datetimeCreated) {
        this.datetimeCreated = datetimeCreated;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    @NonNull
    public String getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(@NonNull String shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public JSONObject toJSONobject() {
        try {
            JSONObject jsonObject = super.toJSONobject();
            jsonObject.put("product", product);
            jsonObject.put("quantity", quantity);
            jsonObject.put("dateTimeCreated", datetimeCreated.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));
            jsonObject.put("authorId", authorId);
            jsonObject.put("finished", finished);
            jsonObject.put("shoppingListId", shoppingListId);
            return jsonObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
