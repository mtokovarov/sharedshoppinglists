from app import app
from config import mysql
from flask import request, make_response, jsonify


allowed_tablenames = ['purchase_plan', 'categories', 'products', 'messages']

def get_fields(conn, table_name):
    sql = f"DESCRIBE {table_name}"
    c = conn.cursor()
    c.execute(sql)
    result = c.fetchall()
    fields = [row['Field'] for row in result]
    return fields

def insert_into_modifications(conn, table_name, row_id, mod_type):
    sql = f"""INSERT INTO modifications (table_name, row_id, modification_type)
            VALUES (%s, %s, %s);"""
    c = conn.cursor()
    c.execute(sql, (table_name, row_id, mod_type))

def insert_data(conn, table_name, data):
    fields = get_fields(conn, table_name)
    c = conn.cursor()
    #insert into tablle_name (fields) values (values)
    fields_str = ", ".join(fields)
    templates =  ", ".join(['%s']*len(fields))

    sql = f"insert into {table_name} ({fields_str}) values ({templates});"
    #print(sql)
    for d in data:
        vals = [d[field] for field in fields]
        try:
            c.execute(sql, vals)
            insert_into_modifications(conn, table_name, d['id'], 'insert')
        except:
            pass
    conn.commit()


def update_data(conn, table_name, data):
    fields = get_fields(conn, table_name)
    c = conn.cursor()
    fields_str = [field + " = %s" for field in fields]
    fields_str = ", ".join(fields_str)
    sql = f"update {table_name} set {fields_str} where id = %s"
    for d in data:
        vals = [d[field] for field in fields] + [d['id']]
        c.execute(sql, vals)
        insert_into_modifications(conn, table_name, d['id'], 'update')
    conn.commit()


def delete_data(conn, table_name, data):
    for d in data:
        insert_into_modifications(conn, table_name, d, 'delete')
    conn.commit()

def select_inserted_data(conn, table_name, last_synch, user_id = None):
    sql = f"""select * from {table_name} where id in
    (select row_id from modifications where table_name = %s and id > %s
     and modification_type = 'insert') and id not in
    (select row_id from modifications where table_name = %s and id > %s
     and modification_type = 'delete')"""


    cursor = conn.cursor()
    args = (table_name, last_synch, table_name, last_synch)


    if(table_name == 'messages'):
        sql += f" and (sender_id = %s or receiver_id = %s);"
        args += (user_id, user_id)

    cursor.execute(sql, args)
    return cursor.fetchall()

def select_updated_data(conn, table_name, last_synch, user_id = None):
    sql = f"""select * from {table_name} where id in
    (select row_id from modifications where table_name = %s and id > %s
     and modification_type = 'update') and id not in
    (select row_id from modifications where table_name = %s and id > %s
     and modification_type = 'delete') and id not in
    (select row_id from modifications where table_name = %s and id > %s
     and modification_type = 'insert')"""
    cursor = conn.cursor()
    args = (table_name, last_synch, table_name, last_synch,
                        table_name, last_synch)

    if(table_name == 'messages'):
        sql += f" and (sender_id = %s or receiver_id = %s);"
        args += (user_id, user_id)
    cursor.execute(sql, args)
    return cursor.fetchall()

def select_deleted_data(conn, table_name, last_synch, user_id  = None):
    sql = f"""select distinct row_id from modifications where table_name = %s and id > %s
     and modification_type = 'delete' and row_id not in
    (select row_id from modifications where table_name = %s and id > %s
     and modification_type = 'insert')"""
    cursor = conn.cursor()
    cursor.execute(sql, (table_name, last_synch, table_name, last_synch))
    return cursor.fetchall()

def get_last_synch(conn, table_name):
    sql = "select if(max(id) is null, 0,max(id)) res from modifications where table_name = %s"
    cursor = conn.cursor()
    cursor.execute(sql, (table_name))
    result = cursor.fetchall()

    return result[0]['res']

@app.route("/synchronization", methods = ['POST'])
def synchronize():
    user_id = '3'
    json_data = request.json
    if (json_data is None):
        return "Wrong request", 400
    
    table_name = json_data['table_name']
    if(table_name not in allowed_tablenames):
        return "Wrong request", 400
    last_synch = json_data['last_synch']
    insert = json_data['insert']
    update = json_data['update']
    delete = json_data['delete']
    
    conn = mysql.connect()
    response_json = {}

    try:
        new_inserted = select_inserted_data(conn, table_name, last_synch, user_id)
        new_updated = select_updated_data(conn, table_name, last_synch, user_id)
        new_deleted = select_deleted_data(conn, table_name, last_synch)
        if (insert is not None):
            try:
                insert_data(conn, table_name, insert)
            except Exception as e:
                print(e)
        if (update is not None):
            try:
                update_data(conn, table_name, update)
            except Exception as e:
                print(e)
        if (delete is not None):
            try:
                delete_data(conn, table_name, delete)
            except Exception as e:
                print(e)

        last_synch = get_last_synch(conn, table_name)
        response_json = {"last_synch":last_synch, "insert":new_inserted,
                     "update":new_updated, "delete":new_deleted}

    #delete
    except Exception as e:
        print(e)
    finally:
        conn.close()
    return jsonify(response_json), 200







if __name__ == "__main__":
    app.run()
